#include <iostream>
#include <thread>
#include <array>
#include <vector>
#include <atomic>

#include "../common/stop_watch.hpp"

//////////////////////////////////////////////////////////////////////

class AtomicCounter {
 public:
  void Increment() {
    value_.fetch_add(1);
  }

  size_t Get() const {
    return value_.load();
  }
 private:
  std::atomic<size_t> value_{0};
};

//////////////////////////////////////////////////////////////////////

static const size_t kCacheLineSize = 64;

//////////////////////////////////////////////////////////////////////

class ShardedCounter {
  static const size_t kShards = 8;

 private:
  class Shard {
   public:
    void Increment() {
      value_.fetch_add(1);
    }

    size_t Get() {
      return value_.load();
    }

   private:
    std::atomic<size_t> value_{0};
  };

 public:
  void Increment() {
    size_t shard_index = GetThisThreadShard();
    shards_[shard_index].Increment();
  }

  size_t Get() {
    size_t value = 0;
    for (size_t i = 0; i < kShards; ++i) {
      value += shards_[i].Get();
    }
    return value;
  }

 private:
  static size_t GetThisThreadShard() {
    static std::hash<std::thread::id> hasher;

    auto tid = std::this_thread::get_id();
    return hasher(tid) % kShards;
  }

 private:
  std::array<Shard, kShards> shards_;
};

//////////////////////////////////////////////////////////////////////

void Stress() {
  //alignas(64) ShardedCounter counter;
  AtomicCounter counter;

  StopWatch stop_watch;

  std::vector<std::thread> threads;
  for (size_t i = 0; i < 10; ++i) {
    threads.emplace_back([&counter]() {
      for (size_t j = 0; j < 1'000'000; ++j) {
        counter.Increment();
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }

  auto elapsed_ms = stop_watch.ElapsedMillis();

  std::cout << counter.Get() << std::endl;

  std::cout << "Elapsed: "
            << elapsed_ms
            << "ms" << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  while (true) {
    Stress();
  }
  return 0;
}
