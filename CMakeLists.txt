cmake_minimum_required(VERSION 3.21)
project(cache_examples)

set(CMAKE_CXX_STANDARD 20)

# Cache-awareness
add_subdirectory(matrix)

# Cache coherence
add_subdirectory(sharded_counter)
add_subdirectory(ring_buffer)
add_subdirectory(spinlock)
