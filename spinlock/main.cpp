#include <iostream>
#include <thread>
#include <vector>
#include <atomic>

#include "../common/stop_watch.hpp"

//////////////////////////////////////////////////////////////////////

// Relax in favour of the CPU owning the lock
// https://c9x.me/x86/html/file_module_x86_id_232.html

inline void SpinLockPause() {
  asm volatile("pause\n" : : : "memory");
}

//////////////////////////////////////////////////////////////////////

// Test-and-Set (TAS) spinlock
class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true)) {  // <-- Cache ping-pong
    }
  }

  void Unlock() {
    locked_.store(false);  // <-- Thundering Herd
  }

 private:
  std::atomic<size_t> locked_{false};
};

//////////////////////////////////////////////////////////////////////

void Stress() {
  SpinLock spinlock;
  size_t counter = 0;  // Guarded by spinlock

  StopWatch stop_watch;

  std::vector<std::thread> threads;

  for (size_t i = 0; i < 10; ++i) {
    threads.emplace_back([&]() {
      // Contender thread
      for (size_t j = 0; j < 100'500; ++j) {
        spinlock.Lock();
        ++counter;  // Critical section
        spinlock.Unlock();
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }

  std::cout << counter << std::endl;

  std::cout << stop_watch.ElapsedMillis() << "ms" << std::endl;
}

int main() {
  while (true) {
    Stress();
  }
  return 0;
}
