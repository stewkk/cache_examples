#include "ring_buffer.hpp"

#include "../common/stop_watch.hpp"

#include <iostream>
#include <thread>
// std::hash
#include <functional>

//////////////////////////////////////////////////////////////////////

// Utilities

//////////////////////////////////////////////////////////////////////

class Backoff {
 public:
  void operator()() {
    std::this_thread::yield();
  }
};

//////////////////////////////////////////////////////////////////////

class Digest {
 public:
  void Feed(int value) {
    digest_ = std::hash<int>()(value) ^ (digest_ << 1);
  }

  size_t Value() const {
    return digest_;
  }

 private:
  size_t digest_ = 0;
};

//////////////////////////////////////////////////////////////////////

void Stress() {
  SPSCRingBuffer<int> buffer(1024);

  for (size_t i = 0; i < 512; ++i) {
    buffer.TryProduce(i);
  }

  static const int kValues = 10'000'000;

  StopWatch stop_watch;

  // Single producer
  std::thread producer([&]() {
    for (int i = 0; i < kValues; ++i) {
      Backoff backoff;
      while (!buffer.TryProduce(i)) {
        backoff();
      }
    }
  });

  Digest digest;

  // Single consumer
  std::thread consumer([&]() {
    for (int i = 0; i < kValues; ++i) {
      Backoff backoff;

      int value;
      while (!buffer.TryConsume(value)) {
        backoff();
      }
      digest.Feed(value);
    }
  });

  producer.join();
  consumer.join();

  std::cout << "Digest = " << digest.Value() << std::endl;

  std::cout << "Elapsed: " << stop_watch.ElapsedMillis() << "ms " << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  while (true) {
    Stress();
  }
  return 0;
};
