#pragma once

#include <atomic>
#include <vector>
#include <new>

//////////////////////////////////////////////////////////////////////

static const size_t kCacheLineSize = 64;

//////////////////////////////////////////////////////////////////////

// Single-Producer/Single-Consumer Fixed-Size Ring Buffer

// https://github.com/rigtorp/SPSCQueue/blob/master/include/rigtorp/SPSCQueue.h
// https://rigtorp.se/ringbuffer/

template <typename T>
class SPSCRingBuffer {
  struct Slot {
    T value;
  };

 public:
  explicit SPSCRingBuffer(const size_t capacity) : buffer_(capacity + 1) {
  }

  bool TryProduce(T value) {
    const size_t curr_tail = tail_.load();
    const size_t curr_head = head_.load();

//    if (Next(curr_tail) == head_cached_) {
//      head_cached_ = head_.load();
//    }
//    const size_t curr_head = head_cached_;

    if (IsFull(curr_head, curr_tail)) {
      return false;
    }

    buffer_[curr_tail].value = std::move(value);
    tail_.store(Next(curr_tail));
    return true;
  }

  bool TryConsume(T& value) {
    const size_t curr_head = head_.load();
    const size_t curr_tail = tail_.load();

//    if (curr_head == tail_cached_) {
//      tail_cached_ = tail_.load();
//    }
//    const size_t curr_tail = tail_cached_;


    if (IsEmpty(curr_head, curr_tail)) {
      return false;
    }

    value = std::move(buffer_[curr_head].value);
    head_.store(Next(curr_head));
    return true;
  }

 private:
  bool IsFull(size_t head, size_t tail) const {
    return Next(tail) == head;
  }

  bool IsEmpty(size_t head, size_t tail) const {
    return tail == head;
  }

  size_t Next(size_t slot) const {
    return (slot + 1) % buffer_.size();
  }

 private:
  std::vector<Slot> buffer_;

  // Owned by producer
  std::atomic<size_t> tail_{0};
  // Owned by consumer
  std::atomic<size_t> head_{0};
};
